# Android App Points - Backend

Android App Points is a Django-based backend application that provides functionality for both admin and user interfaces. Admins can add Android apps and specify the points earned by users for downloading these apps. Users can view apps added by admins, see their profile information, earned points, completed tasks, and upload screenshots to confirm app downloads.

## Table of Contents
- [Features](#features)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [API Endpoints](#api-endpoints)


## Features

### Admin-Facing Features
- Add Android apps with specified points.
- View the list of Android apps added.
- Configure the points earned for each app.

### User-Facing Features
- Signup and login using authentication.
- View their name and profile.
- Check the points earned.
- See the number of tasks completed.
- Upload screenshots to confirm app downloads.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Python 3.11.4 or greater 
- Django
- Django REST framework
- Djoser and JWT is Used 



## Installation

1. Clone the repository:
   ```sh
   git clone https://github.com/yourusername/android-app-points-backend.git

## Create a virtual environment (recommended):
2. 
    ``` 
    python -m venv venv
    source venv/bin/activate  # On Windows, use: venv\Scripts\activate

    ```
## Install project dependencies:

3. ``` 
    pip install -r requirements.txt

    ```

## Apply database migrations:
4.  
    ```
    python manage.py migrate

    ```

## Apply database migrations:
5.  
    ```
    python manage.py runserver
    
    ```
# API Endpoints
### Postman API endPoint Documentation 
https://documenter.getpostman.com/view/28221885/2s9YRGypmv


