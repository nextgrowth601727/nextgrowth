# Generated by Django 4.2.6 on 2023-10-27 14:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_alter_androidapp_app_subcategory'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='task',
            name='description',
        ),
        migrations.RemoveField(
            model_name='userprofile',
            name='profile_picture',
        ),
    ]
