from djoser.serializers import UserCreateSerializer
from .models import CustomUser,AndroidApp, AppCategory, AppSubcategory,UserProfile,Task
from django.contrib.auth import get_user_model
from rest_framework import serializers


User=get_user_model()


# user create serializer
class UserCreateSerializer(UserCreateSerializer):
    class Meta(UserCreateSerializer.Meta):
        model=CustomUser
        fields=('id','name','email','password')
    
    
# admin or user serializer
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields=('id','email','name', 'is_superuser')


class AndroidAppSerializer(serializers.ModelSerializer):
    app_category_name = serializers.CharField(source='app_category.name')
    app_subcategory_name = serializers.CharField(source='app_subcategory.name')

    class Meta:
        model=AndroidApp
        fields = '__all__'


    def create(self, validated_data):
        app_category_data = validated_data.pop('app_category')
        app_subcategory_data = validated_data.pop('app_subcategory')

        app_category, created = AppCategory.objects.get_or_create(**app_category_data)
        app_subcategory, created = AppSubcategory.objects.get_or_create(parent_category=app_category, **app_subcategory_data)

        android_app = AndroidApp.objects.create(app_category=app_category, app_subcategory=app_subcategory, **validated_data)
        return android_app

# app category and subCategory

class AppSubcategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = AppCategory
        fields = '__all__'


class SubcategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = AppSubcategory
        fields = ('id', 'name')





# user point serializer
class PointsSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='user.name') 
    class Meta:
        model = UserProfile
        fields = ('id','user', 'name','points_earned')



# user  Profile
class UserHomeSerializer(serializers.ModelSerializer):
    class Meta:
        model=AndroidApp
        fields=('id','name','app_link','points','appImage')

# user task serializer
class UserTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ('app', 'user', 'screenshot')

    def create(self, validated_data):
        # Get the current user from the context
        current_user = self.context['request'].user
        # Set the 'user' field with the current user
        validated_data['user'] = current_user
        # Create the task with the updated data
        task = Task.objects.create(**validated_data)

        # Increment the user's points with the points associated with the app in the task
        app_points = task.app.points
        current_user_profile = UserProfile.objects.get(user=current_user)
        current_user_profile.points_earned += app_points
        current_user_profile.save()

        return task

#user points
class UserPoints(serializers.ModelSerializer):
    class Meta:
        model=UserProfile
        fields=('__all__')

#user Task compleat
class UserTaskViewSerializer(serializers.ModelSerializer):
    app_name = serializers.CharField(source='app.name') 
    class Meta:
        model=Task
        fields=('app_name','screenshot')
    