from django.contrib import admin
from .models import CustomUser,AndroidApp,UserProfile,Task,AppCategory,AppSubcategory

# Register your models here.

admin.site.register(CustomUser)
admin.site.register(AndroidApp)
admin.site.register(UserProfile)
admin.site.register(Task)
admin.site.register(AppCategory)
admin.site.register(AppSubcategory)

