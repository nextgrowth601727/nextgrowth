from rest_framework import permissions

class IsNonAdminUser(permissions.BasePermission):
    def has_permission(self, request, view):
        # Check if the user is authenticated and is neither a staff user nor a superuser
        return request.user.is_authenticated and not (request.user.is_staff or request.user.is_superuser)
