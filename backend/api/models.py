from django.contrib.auth.models import AbstractBaseUser,PermissionsMixin,BaseUserManager
from django.db import models



class CustomUserManager(BaseUserManager):
    def create_user(self,email,password=None,**extra_fields):
        if not email:
            raise ValueError("The Email field must be set ")
        email=self.normalize_email(email)
        user=self.model(email=email,**extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user
    
    def create_superuser(self,email,password=None,**extra_fields):
        extra_fields.setdefault("is_staff",True)
        extra_fields.setdefault("is_superuser",True)

        return self.create_user(email,password,**extra_fields)




class CustomUser(AbstractBaseUser,PermissionsMixin):
    name=models.CharField(max_length=50)
    email=models.EmailField(unique=True)
    is_active=models.BooleanField(default=True)
    is_staff=models.BooleanField(default=False)



    USERNAME_FIELD="email"
    REQUIRED_FIELDS=[]

    objects=CustomUserManager()

    def __str__(self):
        return self.email
    


class AppCategory(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class AppSubcategory(models.Model):
    name = models.CharField(max_length=255)
    parent_category = models.ForeignKey(AppCategory, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

class AndroidApp(models.Model):
    name = models.CharField(max_length=255)
    app_link = models.TextField()
    app_category = models.ForeignKey(AppCategory, on_delete=models.CASCADE, default=1)  
    app_subcategory = models.ForeignKey(AppSubcategory, on_delete=models.CASCADE, default=1)  
    points = models.PositiveIntegerField()
    appImage=models.ImageField(upload_to="App_images/")

    def __str__(self):
        return self.name

# user


class UserProfile(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    points_earned = models.PositiveIntegerField(default=0)
    tasks_completed = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.user.email
    
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            UserProfile.objects.create(user=instance)

    # Connect the signal to the User model's post_save signal
    models.signals.post_save.connect(create_user_profile, sender=CustomUser)

class Task(models.Model):
    app = models.ForeignKey(AndroidApp, on_delete=models.CASCADE)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    screenshot = models.ImageField(upload_to='task_screenshots/', )

    def __str__(self):
        return self.user.name
