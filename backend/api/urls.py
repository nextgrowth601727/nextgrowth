from django.urls import path
from . import views


urlpatterns = [
    path('check/',views.check_superuser,name='check'),

    # admin endpoints
    path('admin/',views.AndroidAppCreate.as_view(),name='admin'),
    path('admin/user-points/', views.AdminPointsView.as_view(), name='admin-points-view'),
    path('admin/app-categories/', views.AppCategoryList.as_view(), name='app-category-list'),
    path('admin/subcategories/', views.SubcategoryListView.as_view(), name='subcategory-list'),
    # user endpoints
    path('user/',views.UserHomeView.as_view(),name='user-home'),
    path('user/task/',views.UserTaskView.as_view(),name='user-task-upload'),
    path('user/points/',views.UserPointView.as_view(),name="user-points"),
    path('user/task/compleate/',views.UserTaskCompleateView.as_view(),name="user-task-compleate"),
]
