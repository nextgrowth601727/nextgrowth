from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from .serializers import (
    UserSerializer,
    AndroidAppSerializer,
    PointsSerializer,
    UserHomeSerializer,
    UserTaskSerializer,
    AppSubcategorySerializer,
    SubcategorySerializer,
    UserPoints,
    UserTaskViewSerializer
    )
from rest_framework.permissions import IsAdminUser
from rest_framework import generics
from rest_framework.views import APIView
from .permissions import IsNonAdminUser 
from rest_framework.authentication import TokenAuthentication
from .models import AndroidApp,UserProfile,Task,AppCategory,AppSubcategory
from rest_framework import status



# admin
class AndroidAppCreate(generics.ListCreateAPIView):
    queryset=AndroidApp.objects.all()
    serializer_class=AndroidAppSerializer
    permission_classes=[IsAdminUser]

    def list(self, request):
        # Note the use of `get_queryset()` instead of `self.queryset`
        queryset = self.get_queryset()
        serializer = AndroidAppSerializer(queryset, many=True)
        return Response(serializer.data)
    

class AdminPointsView(generics.ListAPIView):
    queryset = UserProfile.objects.filter(tasks_completed__gt=0)  # Filter users who have completed tasks
    serializer_class = PointsSerializer  # Create this serializer for formatting points data
    permission_classes = [IsAdminUser]

# app category and subcategory view
class AppCategoryList(generics.ListAPIView):
    queryset = AppCategory.objects.all()
    serializer_class = AppSubcategorySerializer
    permission_classes=[IsAdminUser]

class SubcategoryListView(generics.ListAPIView):
    serializer_class = SubcategorySerializer

    def get_queryset(self):
        category_id = self.request.query_params.get('category')
        if category_id:
            # Filter subcategories based on the provided category ID
            return AppSubcategory.objects.filter(parent_category_id=category_id)
        return AppSubcategory.objects.all()


# user
class UserHomeView(generics.ListAPIView):
    queryset = AndroidApp.objects.all()
    serializer_class = AndroidAppSerializer
    permission_classes = [IsNonAdminUser]

class UserTaskView(generics.CreateAPIView):
    serializer_class = UserTaskSerializer
    permission_classes = [IsNonAdminUser]

    def perform_create(self, serializer):
        # Set the 'user' field with the current user
        serializer.validated_data['user'] = self.request.user
        serializer.save()

        # Update the user's points based on the task
        app_points = serializer.instance.app.points
        user_profile = UserProfile.objects.get(user=self.request.user)
        user_profile.points_earned += app_points
        user_profile.save()

class UserPointView(generics.ListAPIView):
    serializer_class = UserPoints
    permission_classes=[IsNonAdminUser]

    def get_queryset(self):
        user = self.request.user
        return UserProfile.objects.filter(user=user)


class UserTaskCompleateView(generics.ListAPIView):
    serializer_class=UserTaskViewSerializer
    permission_classes=[IsNonAdminUser]

    def get_queryset(self):
        user = self.request.user
        return Task.objects.filter(user=user)



@api_view(['GET'])
@permission_classes([permissions.IsAuthenticated])
def check_superuser(request):
    user = request.user
    serializer = UserSerializer(user)  # Create a serializer instance with the user object

    if user.is_superuser:

        response_data={
            'user_info':serializer.data
        }

    else:
        response_data={
            'user_info':serializer.data
        }


  

    return Response(response_data)
